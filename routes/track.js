const express = require('express');
const apiRoutes = express.Router();
const TrackModel = require('../models/track');

const passport = require('passport');

apiRoutes.post('/', passport.authenticate('jwt', { session: false }), (request, response) => {
	console.log(request.body);

	new TrackModel(request.body).save((error, doc) => {
		// console.log(doc);
		if (error) response.json(error);
		request.io.emit('updatePosition', doc);
		// response.json(doc);
	});
	
});

// apiRoutes.get('/', (request, response) => {
// 	TrackModel.find({}, (error, docs) => {
// 		response.json(docs[docs.length - 1]);
// 	});
// });

module.exports = apiRoutes;
