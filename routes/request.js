const express = require('express');
const apiRoutes = express.Router();
const Request = require('../models/request');

const passport = require('passport');

apiRoutes.post('/', (request, response) => {
	new Request(request.body).save((error, res) => {
		if (error) response.json(error);

		request.io.emit('StopRequest', res);

		response.json({ 'message': 'Your request has been sent!' });
	});
});

module.exports = apiRoutes;
