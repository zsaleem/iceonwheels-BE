const express = require('express');
const apiRoutes = express.Router();

const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('../config/main');

const User = require('../models/user');

apiRoutes.post('/', function(req, res) {
  User.findOne({
    login: req.body.login
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({ success: false, message: 'Authentication failed. User not found.' });
    } else {
      if (user.password === req.body.password) {
        var token = jwt.sign(user.toJSON(), config.secret, {
          expiresIn: 10080 // in seconds
        });
        res.json({ success: true, token: 'JWT ' + token, login: user.login, id: user._id });
      } else {
        res.send({ success: false, message: 'Authentication failed. Passwords did not match.' });
      }

      // Check if password matches
      // user.comparePassword(req.body.password, function(err, isMatch) {
      //   console.log(isMatch);
      //   if (isMatch && !err) {
      //     console.log('MATHCED');
      //     // Create token if the password matched and no error was thrown
      //     var token = jwt.sign(user.toJSON(), config.secret, {
      //       expiresIn: 10080 // in seconds
      //     });
      //     res.json({ success: true, token: 'JWT ' + token });
      //   } else {
      //     console.log('UNMATCHED');
      //     res.send({ success: false, message: 'Authentication failed. Passwords did not match.' });
      //   }
      // });
    }
  });
});

module.exports = apiRoutes;
