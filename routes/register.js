const express = require('express');
const apiRoutes = express.Router(); 

const User = require('../models/user');

// Register new users
apiRoutes.post('/', (req, res) => {
  if (!req.body.email || !req.body.password) {
    res.json({ success: false, message: 'Please enter email and password.' });
  } else {
    let newUser = new User({
      email: req.body.email,
      password: req.body.password
    });

    // Attempt to save the user
    newUser.save((err) => {
      if (err) {
        return res.json({ success: false, message: 'That email address already exists.' });
      }
      res.json({ success: true, message: 'Successfully created new user.' });
    });
  }
});

module.exports = apiRoutes;
