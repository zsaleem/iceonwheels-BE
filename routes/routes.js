const express = require('express');
const apiRoutes = express.Router();

const RoutesModel = require('../models/routes');

const passport = require('passport');

apiRoutes.post('/', passport.authenticate('jwt', { session: false }), (request, response) => {
	RoutesModel.find({}, (error, docs) => {
		request.body['title'] = 'Routes ' + docs.length;
		request.body['distance'] = Math.floor(request.body.distance);

		new RoutesModel(request.body).save((error, doc) => {
			if (error) console.log(error);

			response.json(doc);
		});
	});
});

apiRoutes.get('/path', (request, response) => {
	RoutesModel.findOne({ active: true }, (error, docs) => {
		response.json(docs);
	});
});

apiRoutes.get('/tracks', passport.authenticate('jwt', { session: false }), (request, response) => {
	RoutesModel.find({}, (error, docs) => {
		response.json(docs);
	});
});

apiRoutes.post('/delete', passport.authenticate('jwt', { session: false }), (request, response) => {
	RoutesModel.findByIdAndRemove(request.body.id, (error, doc) => {
		if (error) response.json(error);

		if (doc) {
			response.json('Route Deleted');
		}
	});
});

module.exports = apiRoutes;
