const express = require('express');
const apiRoutes = express.Router();

const passport = require('passport');

apiRoutes.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.send('It worked! User id is: ' + req.user._id + '.');
});

module.exports = apiRoutes;
