const express = require('express');
const apiRoutes = express.Router();
const Status = require('../models/status');

const passport = require('passport');

apiRoutes.post('/', passport.authenticate('jwt', { session: false }), (request, response) => {
	new Status(request.body).save((error, doc) => {
		// request.io.on('NewPosition', data => {
		// 	console.log('New Position');
		// 	request.io.emit('SendPosition', data);
		// });

		console.log('POST STATUS');

		response.json(doc);
	});
});

apiRoutes.get('/', (request, response) => {
	Status.find({}, (error, docs) => {
		if (error) throw error;
		// console.log(docs);
		if (docs.length == 0) {
			response.json(false);	
		} else {
			response.json(docs[docs.length - 1].status);
		}
	});
});

module.exports = apiRoutes;
