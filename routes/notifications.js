const express = require('express');
const apiRoutes = express.Router();
const crypto = require('crypto');

const Notification = require('../models/notification');

apiRoutes.get('/', function(request, response) {
	Notification.find({ userid: request.body.userid }, (error, doc) => {
    if (error) response.json(error);
    response.json(doc);
  });
});

apiRoutes.post('/', function(request, response) {
	Notification.findOneAndUpdate({ token: request.body.token }, request.body, { new: true }, (error, doc) => {
    if (error) response.json(error);
    response.json({ success: true, message: 'Notifications Updated Successfully.' });
  });
});

apiRoutes.post('/token', function(request, response) {
	let token = crypto.randomBytes(50).toString('hex');

	new Notification({ token: token, route: true, proximity: true, arrival: true }).save((error, res) => {
		if (error) response.json(error);
		response.json({ success: true, message: 'Notifications token generated.', notificationToken: res.token });
	});
});

module.exports = apiRoutes;
