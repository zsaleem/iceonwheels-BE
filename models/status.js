const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const StatusSchema = new mongoose.Schema({
	date: {
		type: Date
	},
  status: {
    type: Boolean
  }
});

module.exports = mongoose.model('Status', StatusSchema);
