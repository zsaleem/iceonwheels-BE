const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const RoutesSchema = new mongoose.Schema({
  title: {
    type: String
  },
	date: {
		type: Date
	},
  path: {
  	type: Array
  },
  distance: {
    type: Number
  },
  active: {
  	type: Boolean
  }
});

module.exports = mongoose.model('Routes', RoutesSchema);
