const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
	date: {
		type: Date
	},
  lat: {
    type: Number
  },
  lon: {
    type: Number
  }
});

module.exports = mongoose.model('Track', TrackSchema);
