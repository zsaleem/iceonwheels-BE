const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const RequestSchema = new mongoose.Schema({
	date: {
		type: Date
	},
  name: {
  	type: String
  },
  mobile: {
    type: Number
  },
  street: {
    type: Number
  },
  lat: {
  	type: Number
  },
  lon: {
  	type: Number
  }
});

module.exports = mongoose.model('Request', RequestSchema);
