const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const NotificationSchema = new mongoose.Schema({
	token: {
		type: String
	},
  route: {
    type: Boolean
  },
  proximity: {
    type: Boolean
  },
  arrival: {
    type: Boolean
  }
});

module.exports = mongoose.model('Notification', NotificationSchema);
