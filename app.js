const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const passport = require('passport');
const config = require('./config/main');
const cors = require('cors');

const authenticate = require('./routes/authenticate');
const notifications = require('./routes/notifications');
const contact = require('./routes/contact');
const request = require('./routes/request');
const status = require('./routes/status');
const route = require('./routes/routes');
const track = require('./routes/track');

const app = express();

app.use((request, response, next) => {
  // response.header('Access-Control-Allow-Origin', '*');
  // response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  request.io = io;

  next();
});

// app.use('/uploads', express.static('uploads'));

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

// const server = app.listen(3000, '192.168.2.12');
const server = app.listen(3000);

const http = require('http').Server(app);
const io = require('socket.io').listen(server);

io.on('updatePosition', data => {
	console.log('ajkshdkashdkjashdkjas');
	console.log(data);
});

console.log('Listening to port 3000');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Log requests to console
app.use(morgan('dev'));

app.use(passport.initialize());
app.use(cors());

mongoose.connect(config.database);
require('./config/passport')(passport);

// Set url for API group routes
app.use('/api/1.0/authenticate', authenticate);
app.use('/api/1.0/notifications', notifications);
app.use('/api/1.0/contact', contact);
app.use('/api/1.0/request', request);
app.use('/api/1.0/status', status);
app.use('/api/1.0/routes', route);
app.use('/api/1.0/track', track);
// app.use('/api/1.0/routes/position', position);

